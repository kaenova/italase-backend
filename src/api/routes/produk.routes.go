package routes

import (
	"src/api/controllers"
	"src/utils"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func Produk(e *echo.Echo) *echo.Echo {

	/*
		Contoh setup controller
		e.GET("/user", controller.UserGet)

		e.PUT("/user", controller.UserPut)
		e.DELETE("/user", controller.UserDelete)
	*/
	g := e.Group("/produk")
	g.Use(middleware.JWTWithConfig(utils.JWTconfig))
	g.POST("", controllers.ProdukPost)
	g.GET("", controllers.ProdukGet)
	g.DELETE("", controllers.ProdukDelete)
	g.PUT("", controllers.ProdukUpdate)

	return e
}
