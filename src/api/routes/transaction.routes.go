package routes

import (
	"src/api/controllers"
	"src/utils"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func Transaction(e *echo.Echo) *echo.Echo {

	/*
		Contoh setup controller
		e.GET("/user", controller.UserGet)

		e.PUT("/user", controller.UserPut)
		e.DELETE("/user", controller.UserDelete)
	*/
	g := e.Group("/transaction")
	g.Use(middleware.JWTWithConfig(utils.JWTconfig))
	g.POST("", controllers.TransactionBatchPost)
	g.GET("", controllers.TransactionBatchGet)
	g.DELETE("", controllers.TransactionBatchDelete)
	g.PUT("", controllers.TransactionBatchPut)

	return e
}
