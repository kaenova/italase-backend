package routes

import (
	"src/api/controllers"
	"src/utils"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func Hutang(e *echo.Echo) *echo.Echo {

	g := e.Group("/hutang")
	g.Use(middleware.JWTWithConfig(utils.JWTconfig))
	// g.POST("", controllers.HutangPost)
	g.GET("", controllers.HutangGet)
	g.DELETE("", controllers.HutangDelete)
	g.PUT("", controllers.HutangPut)

	return e
}
