package routes

import (
	"src/api/controllers"
	"src/utils"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func Stats(e *echo.Echo) *echo.Echo {

	/*
		Contoh setup controller
		e.GET("/user", controller.UserGet)

		e.PUT("/user", controller.UserPut)
		e.DELETE("/user", controller.UserDelete)
	*/
	e.GET("/stats/transaction", controllers.TransactionStatsGet, middleware.JWTWithConfig(utils.JWTconfig))
	e.GET("/stats/hutang", controllers.HutangStatsGet, middleware.JWTWithConfig(utils.JWTconfig))

	return e
}
