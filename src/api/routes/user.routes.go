package routes

import (
	"src/api/controllers"
	"src/utils"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
)

func Auth(e *echo.Echo) *echo.Echo {
	// jwt/authorisasi
	u := e.Group("/user")
	u.Use(middleware.JWTWithConfig(utils.JWTconfig))
	u.GET("", controllers.GetUser)
	u.PUT("", controllers.UpdateUser)
	u.DELETE("", controllers.DeleteUser)

	// publik akses
	e.POST("/user", controllers.PostUser) //Creating Regular Account
	// auth middleware
	e.POST("/login", controllers.LoginUser)

	return e
}
