package models

import (
	"errors"
	"fmt"
	"src/db"
	"src/entity"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

func CreateTransactionBatch(req *entity.TransactionBatch, reqHutang *entity.Hutang) error {
	db := db.GetDB()
	req.ID = uuid.New()

	tx := db.Begin()
	tx.SavePoint("sp1")

	// Create Hutang
	if (*reqHutang != entity.Hutang{}) {
		if err := tx.Create(&reqHutang).Error; err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return errors.New("transaction fail " + err.Error())
		}
		req.HutangID = &reqHutang.ID
	}

	// Create all small transaction
	for i := 0; i < len(req.Transactions); i++ {
		// Get produk by id
		produkBefore, err := GetProdukID(req.Transactions[i].ProdukID)
		if err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return errors.New("transaction fail " + err.Error())
		}
		// Update Produk
		produkNow := produkBefore
		produkNow.JumlahStok = uint(req.Transactions[i].JumlahAkhir)
		if err := tx.Model(&produkNow).Select("jumlah_stok").Updates(produkNow).Error; err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return errors.New("transaction fail " + err.Error())
		}
		// Buat Transaksi yg terhubung dengan batch transaksi
		req.Transactions[i].NamaProduk = produkNow.Nama
		req.Transactions[i].HargaJual = produkNow.HargaJual
		req.Transactions[i].BatchID = req.ID
		req.Transactions[i].BesarPerubahan = int(produkNow.JumlahStok) - int(produkBefore.JumlahStok)
		req.Transactions[i].JumlahAkhir = int(produkNow.JumlahStok)
	}
	// Create batch transaction
	if (*reqHutang != entity.Hutang{}) {
		if err := tx.Create(&req).Error; err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return errors.New("transaction fail " + err.Error())
		}
		req.Hutang = reqHutang
	} else {
		req.Hutang = nil
		req.HutangID = nil
		if err := tx.Create(&req).Error; err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return errors.New("transaction fail " + err.Error())
		}
	}

	tx.Commit()
	return nil
}

func BatchTransactionSearchbyID(userID uuid.UUID, batchUUID uuid.UUID) (entity.TransactionBatch, error) {
	var obj entity.TransactionBatch
	obj.ID = batchUUID
	obj.UserID = userID
	db := db.GetDB()
	if err := db.Preload("Hutang").Preload("Transactions").Preload("Transactions.Produk").Where("id = ? AND user_id = ? AND deleted_at is NULL", batchUUID, userID).First(&obj).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return entity.TransactionBatch{}, nil
		}
		return entity.TransactionBatch{}, errors.New("transaction fail " + err.Error())
	}

	return obj, nil
}

func BatchTransactionPaginationbyUserID(userID uuid.UUID, page int) ([]entity.TransactionBatch, error) {
	var (
		objs       []entity.TransactionBatch
		numPerPage int = 10 // Hardcoded 10 kembalian per request
	)

	db := db.GetDB()
	offset := (page - 1) * numPerPage

	if tx := db.Preload("Hutang").Order("waktu_transaksi desc").Where("user_id = ?", userID).Offset(offset).Limit(numPerPage).Find(&objs); tx.Error != nil {
		if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
			return []entity.TransactionBatch{}, nil
		}
		return objs, errors.New("transaction fail")
	}
	return objs, nil
}

func BatchTransactionSearchCatatan(cat string, user uuid.UUID, page int) ([]entity.TransactionBatch, error) {
	var (
		objs       []entity.TransactionBatch
		numPerPage int = 10 // Hardcoded 10 kembalian per request
	)

	db := db.GetDB()
	offset := (page - 1) * numPerPage

	tx := db.Preload("Hutang").Order("waktu_transaksi desc").Where("LOWER(catatan) LIKE LOWER(?) AND user_id = ?", fmt.Sprintf("%%%s%%", cat), user).Offset(offset).Limit(numPerPage).Find(&objs)
	if tx.Error != nil {
		if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
			return []entity.TransactionBatch{}, nil
		}
		return []entity.TransactionBatch{}, tx.Error
	}

	return objs, nil
}

func BatchTransactionToday(user uuid.UUID, page int) ([]entity.TransactionBatch, error) {
	// https://play.golang.org/p/jnFuZxruKm
	var (
		objs       []entity.TransactionBatch
		numPerPage int = 10 // Hardcoded 10 kembalian per request
	)
	now := time.Now()
	rounded := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
	max := rounded.AddDate(0, 0, 1)

	db := db.GetDB()
	offset := (page - 1) * numPerPage

	if err := db.Preload("Hutang").Order("waktu_transaksi desc").Where("waktu_transaksi >= ? AND waktu_transaksi < ? AND user_id = ?", rounded, max, user).Offset(offset).Limit(numPerPage).Find(&objs).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return []entity.TransactionBatch{}, nil
		}
		return []entity.TransactionBatch{}, err
	}
	return objs, nil
}

func BatchTransactionWeek(user uuid.UUID, page int) ([]entity.TransactionBatch, error) {
	// https://play.golang.org/p/jnFuZxruKm
	var (
		objs       []entity.TransactionBatch
		numPerPage int = 10 // Hardcoded 10 kembalian per request
	)
	now := time.Now()
	// -1 karena dimulai dari Senin
	rounded := time.Date(now.Year(), now.Month(), now.Day()-(int(now.Weekday())-1), 0, 0, 0, 0, now.Location())
	max := rounded.AddDate(0, 0, 7)

	db := db.GetDB()
	offset := (page - 1) * numPerPage

	if err := db.Preload("Hutang").Order("waktu_transaksi desc").Where("waktu_transaksi >= ? AND waktu_transaksi < ? AND user_id = ?", rounded, max, user).Offset(offset).Limit(numPerPage).Find(&objs).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return []entity.TransactionBatch{}, nil
		}
		return []entity.TransactionBatch{}, err
	}
	return objs, nil
}

func BatchTransactionBulan(user uuid.UUID, page int) ([]entity.TransactionBatch, error) {
	// https://play.golang.org/p/jnFuZxruKm
	var (
		objs       []entity.TransactionBatch
		numPerPage int = 10 // Hardcoded 10 kembalian per request
	)
	now := time.Now()
	rounded := time.Date(now.Year(), now.Month(), 0, 0, 0, 0, 0, now.Location())
	max := rounded.AddDate(0, 1, 0)

	db := db.GetDB()
	offset := (page - 1) * numPerPage

	if err := db.Preload("Hutang").Order("waktu_transaksi desc").Where("waktu_transaksi >= ? AND waktu_transaksi < ? AND user_id = ?", rounded, max, user).Offset(offset).Limit(numPerPage).Find(&objs).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return []entity.TransactionBatch{}, nil
		}
		return []entity.TransactionBatch{}, err
	}
	return objs, nil
}

func BatchTransactionSatuTanggal(waktu time.Time, user uuid.UUID, page int) ([]entity.TransactionBatch, error) {
	// https://play.golang.org/p/jnFuZxruKm
	var (
		objs       []entity.TransactionBatch
		numPerPage int = 10 // Hardcoded 10 kembalian per request
	)
	rounded := time.Date(waktu.Year(), waktu.Month(), waktu.Day(), 0, 0, 0, 0, waktu.Location())
	max := rounded.AddDate(0, 0, 1)

	db := db.GetDB()
	offset := (page - 1) * numPerPage

	if err := db.Preload("Hutang").Order("waktu_transaksi desc").Where("waktu_transaksi >= ? AND waktu_transaksi < ? AND user_id = ?", rounded, max, user).Offset(offset).Limit(numPerPage).Find(&objs).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return []entity.TransactionBatch{}, nil
		}
		return []entity.TransactionBatch{}, err
	}
	return objs, nil
}

func BatchTransactionRangeTanggal(waktuBawah time.Time, waktuAtas time.Time, user uuid.UUID, page int) ([]entity.TransactionBatch, error) {
	// https://play.golang.org/p/jnFuZxruKm
	var (
		objs       []entity.TransactionBatch
		numPerPage int = 10 // Hardcoded 10 kembalian per request
	)
	roundedBawah := time.Date(waktuBawah.Year(), waktuBawah.Month(), waktuBawah.Day(), 0, 0, 0, 0, waktuBawah.Location())
	roundedAtas := time.Date(waktuAtas.Year(), waktuAtas.Month(), waktuAtas.Day(), 0, 0, 0, 0, waktuAtas.Location())
	roundedAtas = roundedAtas.AddDate(0, 0, 1)

	db := db.GetDB()
	offset := (page - 1) * numPerPage

	if err := db.Preload("Hutang").Order("waktu_transaksi desc").Where("waktu_transaksi >= ? AND waktu_transaksi < ? AND user_id = ?", roundedBawah, roundedAtas, user).Offset(offset).Limit(numPerPage).Find(&objs).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return []entity.TransactionBatch{}, nil
		}
		return []entity.TransactionBatch{}, err
	}
	return objs, nil
}

func DeleteTransactionID(id uuid.UUID, user uuid.UUID) (entity.TransactionBatch, error) {
	var tempObj entity.TransactionBatch
	db := db.GetDB()

	// Get the object
	err := db.Preload("Transactions").Where("id = ? AND user_id = ?", id, user).First(&tempObj).Error
	if err != nil {
		return entity.TransactionBatch{}, err
	}

	tx := db.Begin()
	tx.SavePoint("sp1")

	// Delete Hutang
	if tempObj.HutangID != nil {
		var objTemp entity.Hutang
		objTemp.ID = *tempObj.HutangID
		if err := tx.Delete(&objTemp).Error; err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return entity.TransactionBatch{}, errors.New("transaction fail")
		}
	}
	// Delete transaksi kecil
	for i := 0; i < len(tempObj.Transactions); i++ {
		if err := tx.Delete(&tempObj.Transactions[i]).Error; err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return entity.TransactionBatch{}, err
		}
	}

	// Delete transaksi besar
	err = tx.Clauses(clause.Returning{}).Delete(&tempObj).Error
	if err != nil || errors.Is(tx.Error, gorm.ErrRecordNotFound) {
		tx.RollbackTo("sp1")
		tx.Commit()
		return entity.TransactionBatch{}, errors.New("transaction fail")
	}

	tx.Commit()
	return tempObj, nil
}

func getIndexProdukInObj(a entity.Transaction, b []entity.Transaction) int {
	for i := 0; i < len(b); i++ {
		if a.ProdukID == b[i].ProdukID {
			return i
		}
	}
	return -1
}

func UpdateBatchTransaction(req entity.TransactionBatch, NamaPelanggan string) (entity.TransactionBatch, error) {
	var (
		objs    []entity.Transaction
		allData entity.TransactionBatch
	)

	db := db.GetDB()

	// Get all the data
	allData.ID = req.ID
	err := db.Preload("Transactions").Preload("Hutang").Where("id = ? AND user_id = ?", req.ID, req.UserID).First(&allData).Error
	if err != nil {
		return entity.TransactionBatch{}, err
	}

	tx := db.Begin()
	tx.SavePoint("sp1")

	// Remove The Transaction that has been made (rollback again)
	objs = allData.Transactions
	for i := 0; i < len(objs); i++ {
		err = tx.Unscoped().Delete(&objs[i]).Error
		if err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return entity.TransactionBatch{}, errors.New("transaction fail " + err.Error())
		}
	}
	// Revert to normal value again on produk
	for i := 0; i < len(objs); i++ {
		produkRevert, err := GetProdukID(objs[i].ProdukID)
		if err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return entity.TransactionBatch{}, errors.New("transaction fail " + err.Error())
		}
		produkRevert.JumlahStok = produkRevert.JumlahStok - uint(objs[i].BesarPerubahan)
		err = db.Select("jumlah_stok").Save(&produkRevert).Error
		if err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return entity.TransactionBatch{}, errors.New("transaction fail " + err.Error())
		}
	}

	// Create all small transaction
	for i := 0; i < len(req.Transactions); i++ {
		// Get produk by id
		produkBefore, err := GetProdukID(req.Transactions[i].ProdukID)
		if err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return entity.TransactionBatch{}, errors.New("transaction fail " + err.Error())
		}
		// Update Produk
		produkNow := produkBefore
		produkNow.JumlahStok = uint(req.Transactions[i].JumlahAkhir)
		if err := tx.Select("jumlah_stok").Model(&produkNow).Updates(produkNow).Error; err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return entity.TransactionBatch{}, errors.New("transaction fail " + err.Error())
		}
		// Buat Transaksi yg terhubung dengan batch transaksi
		if idx := getIndexProdukInObj(req.Transactions[i], objs); idx != -1 {
			req.Transactions[i].NamaProduk = objs[idx].NamaProduk
			req.Transactions[i].HargaJual = objs[idx].HargaJual
		} else {
			req.Transactions[i].NamaProduk = produkNow.Nama
			req.Transactions[i].HargaJual = produkNow.HargaJual
		}
		req.Transactions[i].BatchID = req.ID
		req.Transactions[i].BesarPerubahan = int(produkNow.JumlahStok) - int(produkBefore.JumlahStok)
		req.Transactions[i].JumlahAkhir = int(produkNow.JumlahStok)
	}
	// Update if there's a hutnag
	if allData.HutangID != nil && allData.Hutang != nil {
		hutang := allData.Hutang
		hutang.JumlahPeminjaman = req.Penjualan
		hutang.ID = *allData.HutangID
		if !req.IsLunas {
			hutang.NamaPelanggan = NamaPelanggan
		}
		if err := tx.Save(&hutang).Error; err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return entity.TransactionBatch{}, errors.New("transaction fail " + err.Error())
		}
		req.Hutang = hutang
		req.HutangID = &hutang.ID
	}

	// Update if from not Hutang to Hutang (Lunas is must be not true to true)
	if (!req.IsLunas) && (allData.IsLunas) {
		var hutang entity.Hutang
		hutang.ID = uuid.New()
		hutang.NamaPelanggan = NamaPelanggan
		hutang.Catatan = req.Catatan
		hutang.IsMemberi = true
		hutang.JumlahPeminjaman = req.Penjualan
		hutang.UserID = allData.UserID
		hutang.WaktuTransaksi = req.WaktuTransaksi
		req.Hutang = &hutang
		req.HutangID = &hutang.ID
		req.IsLunas = false
	}

	// Jika pelunasan hutang dilakukan melalui edit data
	if (req.IsLunas) && (!allData.IsLunas) {
		req.Hutang = nil
		req.HutangID = nil
	}

	if err := tx.Save(&req).Error; err != nil {
		tx.RollbackTo("sp1")
		tx.Commit()
		return entity.TransactionBatch{}, errors.New("transaction fail " + err.Error())
	}

	// Jika pelunasan hutang dilakukan melalui edit data (Hapus Hutang)
	if (req.IsLunas) && (!allData.IsLunas) {
		if allData.HutangID != nil && allData.Hutang != nil {
			if err := tx.Delete(&allData.Hutang).Error; err != nil {
				return entity.TransactionBatch{}, errors.New("transaction fail " + err.Error())
			}
		}
	}

	tx.Commit()
	return req, nil
}
