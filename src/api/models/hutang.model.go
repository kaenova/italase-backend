package models

import (
	"errors"
	"fmt"
	"src/db"
	"src/entity"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type statsHutang struct {
	Menerima float32 `json:"total_menerima"`
	Memberi  float32 `json:"total_memberi"`
}

func AddHutang(obj *entity.Hutang) error {
	db := db.GetDB()
	if err := db.Select(
		"id", "nama_pelanggan", "catatan", "jumlah_peminjaman", "is_memberi", "waktu_transaksi", "user_id").Create(&obj).Error; err != nil {
		return err
	}
	return nil
}

func HutangStats(user uuid.UUID) (statsHutang, error) {
	db := db.GetDB()
	var stats statsHutang

	// Kalau gasalah ini bisa lebih diefektifkan lagi, lupa caranya -Kaenova

	if err := db.Table("hutang").Select("sum(jumlah_peminjaman) as menerima").Where("user_id = ? AND is_memberi = false AND deleted_at is NULL", user).Scan(&stats).Error; err != nil {
		return statsHutang{}, err
	}
	if err := db.Table("hutang").Select("sum(jumlah_peminjaman) as memberi").Where("user_id = ? AND is_memberi = true AND deleted_at is NULL", user).Scan(&stats).Error; err != nil {
		return statsHutang{}, err
	}
	return stats, nil
}

func HutangSearchbyID(user uuid.UUID, hutangID uuid.UUID) (entity.Hutang, error) {
	db := db.GetDB()

	var obj entity.Hutang

	if err := db.Where("user_id = ? AND id = ?", user, hutangID).First(&obj).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return entity.Hutang{}, nil
		}
		return entity.Hutang{}, err
	}
	return obj, nil
}

func HutangSearchbyNama(src string, user uuid.UUID, pageNum int) ([]entity.Hutang, error) {
	db := db.GetDB()

	var objs []entity.Hutang
	var numPerPage int = 10 // Hardcoded 10 kembalian per request
	offset := (pageNum - 1) * numPerPage

	tx := db.Order("nama_pelanggan asc").Where("LOWER(nama_pelanggan) LIKE LOWER(?) AND user_id = ?", fmt.Sprintf("%%%s%%", src), user).Offset(offset).Limit(numPerPage).Find(&objs)
	if tx.Error != nil {
		if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
			return []entity.Hutang{}, nil
		}
		return []entity.Hutang{}, tx.Error
	}

	return objs, nil
}

func HutangPagination(userID uuid.UUID, page int) ([]entity.Hutang, error) {
	var (
		objs       []entity.Hutang
		numPerPage int = 10 // Hardcoded 10 kembalian per request
	)

	db := db.GetDB()
	offset := (page - 1) * numPerPage

	if tx := db.Order("waktu_transaksi desc").Where("user_id = ?", userID).Offset(offset).Limit(numPerPage).Find(&objs); tx.Error != nil {
		if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
			return []entity.Hutang{}, nil
		}
		return objs, errors.New("transaction fail")
	}
	return objs, nil
}

func HutangDelete(userID uuid.UUID, hutangID uuid.UUID) (entity.Hutang, error) {
	var obj entity.Hutang
	var transaction entity.TransactionBatch
	obj.ID = hutangID

	db := db.GetDB()

	err := db.Table("transaksi_batch").Where("hutang_id = ?", hutangID).First(&transaction).Error
	if err != nil && !errors.Is(gorm.ErrRecordNotFound, err) {
		return entity.Hutang{}, err
	}

	tx := db.Begin()
	tx.SavePoint("sp1")

	// Jika ada pada suatu transaksi, maka ubah transaksi itu dulu
	if transaction.ID != uuid.Nil {
		fmt.Println("MASUKKK!")
		err := tx.Model(&transaction).Select("hutang_id", "is_lunas").Where("user_id = ?", userID).Updates(entity.TransactionBatch{HutangID: nil, IsLunas: true}).Error
		if err != nil {
			tx.RollbackTo("sp1")
			tx.Commit()
			return entity.Hutang{}, err
		}
	}

	if err := tx.Where("user_id = ?", userID).Delete(&obj).Error; err != nil {
		tx.RollbackTo("sp1")
		tx.Commit()
		return entity.Hutang{}, err
	}

	tx.Commit()
	return obj, nil
}

func HutangUbah(req entity.Hutang) (entity.Hutang, error) {
	var obj entity.Hutang = req
	db := db.GetDB()
	if err := db.Select(
		"id", "nama_pelanggan", "catatan", "jumlah_peminjaman", "is_memberi", "waktu_transaksi", "user_id").Save(&obj).Error; err != nil {
		return entity.Hutang{}, nil
	}
	return obj, nil
}

// func HutangLunas(id uuid.UUID, user uuid.UUID) (entity.Hutang, error) {
// 	var trx entity.TransactionBatch
// 	var hutang *entity.Hutang
// 	db := db.GetDB()
// 	tx := db.Begin()
// 	tx.SavePoint("sp1")
// 	// Update Transaksi
// 	err := db.Where("hutang_id = ? AND user_id = ?", id, user).Preload("Hutang").First(&trx).Error
// 	if err != nil {
// 		tx.Commit()
// 		return entity.Hutang{}, errors.New("not authorized")
// 	}
// 	hutang = trx.Hutang
// 	if hutang == nil {
// 		tx.Commit()
// 		return entity.Hutang{}, errors.New("error on getting hutang")
// 	}
// 	trx.Hutang = nil
// 	trx.HutangID = nil

// 	if err := tx.Save(&trx).Error; err != nil {
// 		tx.RollbackTo("sp1")
// 		tx.Commit()
// 		return entity.Hutang{}, err
// 	}

// 	// Delete Hutang
// 	if err := tx.Clauses(clause.Returning{}).Delete(&hutang).Error; err != nil {
// 		tx.RollbackTo("sp1")
// 		tx.Commit()
// 		return entity.Hutang{}, err
// 	}

// 	tx.Commit()
// 	return *hutang, nil
// }
