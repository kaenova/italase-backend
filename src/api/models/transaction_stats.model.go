package models

import (
	"errors"
	"fmt"
	"src/db"
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type statsTransaction struct {
	Pengeluaran float32 `json:"total_pengeluaran"`
	Penjualan   float32 `json:"total_penjualan"`
}

func StatsTransactionSearchCatatan(cat string, user uuid.UUID) (statsTransaction, error) {
	var (
		obj statsTransaction
	)
	db := db.GetDB()
	tx := db.Table("transaksi_batch").Select("sum(penjualan) as penjualan, sum(pengeluaran) as pengeluaran").Where("LOWER(catatan) LIKE LOWER(?) AND user_id = ? AND deleted_at is NULL", fmt.Sprintf("%%%s%%", cat), user).Scan(&obj)
	if tx.Error != nil {
		if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
			return statsTransaction{}, nil
		}
		return statsTransaction{}, tx.Error
	}
	return obj, nil
}

func StatsTransactionToday(user uuid.UUID) (statsTransaction, error) {
	// https://play.golang.org/p/jnFuZxruKm
	var (
		objs statsTransaction
	)
	now := time.Now()
	rounded := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, now.Location())
	max := rounded.AddDate(0, 0, 1)

	db := db.GetDB()

	if err := db.Table("transaksi_batch").Select("sum(penjualan) as penjualan, sum(pengeluaran) as pengeluaran").Where("waktu_transaksi >= ? AND waktu_transaksi < ? AND user_id = ? AND deleted_at is NULL", rounded, max, user).Scan(&objs).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return statsTransaction{}, nil
		}
		return statsTransaction{}, err
	}
	return objs, nil
}

func StatsTransactionWeek(user uuid.UUID) (statsTransaction, error) {
	// https://play.golang.org/p/jnFuZxruKm
	var (
		objs statsTransaction
	)
	now := time.Now()
	// -1 karena dimulai dari Senin
	rounded := time.Date(now.Year(), now.Month(), now.Day()-(int(now.Weekday())-1), 0, 0, 0, 0, now.Location())
	max := rounded.AddDate(0, 0, 7)

	db := db.GetDB()

	if err := db.Table("transaksi_batch").Select("sum(penjualan) as penjualan, sum(pengeluaran) as pengeluaran").Where("waktu_transaksi >= ? AND waktu_transaksi < ? AND user_id = ? AND deleted_at is NULL", rounded, max, user).Scan(&objs).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return statsTransaction{}, nil
		}
		return statsTransaction{}, err
	}
	return objs, nil
}

func StatsTransactionBulan(user uuid.UUID) (statsTransaction, error) {
	// https://play.golang.org/p/jnFuZxruKm
	var (
		objs statsTransaction
	)
	now := time.Now()
	rounded := time.Date(now.Year(), now.Month(), 0, 0, 0, 0, 0, now.Location())
	max := rounded.AddDate(0, 1, 0)

	db := db.GetDB()

	if err := db.Table("transaksi_batch").Select("sum(penjualan) as penjualan, sum(pengeluaran) as pengeluaran").Where("waktu_transaksi >= ? AND waktu_transaksi < ? AND user_id = ? AND deleted_at is NULL", rounded, max, user).Scan(&objs).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return statsTransaction{}, nil
		}
		return statsTransaction{}, err
	}
	return objs, nil
}

func StatsTransactionSatuTanggal(waktu time.Time, user uuid.UUID) (statsTransaction, error) {
	// https://play.golang.org/p/jnFuZxruKm
	var (
		objs statsTransaction
	)
	rounded := time.Date(waktu.Year(), waktu.Month(), waktu.Day(), 0, 0, 0, 0, waktu.Location())
	max := rounded.AddDate(0, 0, 1)

	db := db.GetDB()

	if err := db.Table("transaksi_batch").Select("sum(penjualan) as penjualan, sum(pengeluaran) as pengeluaran").Where("waktu_transaksi >= ? AND waktu_transaksi < ? AND user_id = ? AND deleted_at is NULL", rounded, max, user).Scan(&objs).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return statsTransaction{}, nil
		}
		return statsTransaction{}, err
	}
	return objs, nil
}

func StatsTransactionRangeTanggal(waktuBawah time.Time, waktuAtas time.Time, user uuid.UUID) (statsTransaction, error) {
	// https://play.golang.org/p/jnFuZxruKm
	var (
		objs statsTransaction
	)
	roundedBawah := time.Date(waktuBawah.Year(), waktuBawah.Month(), waktuBawah.Day(), 0, 0, 0, 0, waktuBawah.Location())
	roundedAtas := time.Date(waktuAtas.Year(), waktuAtas.Month(), waktuAtas.Day(), 0, 0, 0, 0, waktuAtas.Location())
	roundedAtas = roundedAtas.AddDate(0, 0, 1)

	db := db.GetDB()

	if err := db.Table("transaksi_batch").Select("sum(penjualan) as penjualan, sum(pengeluaran) as pengeluaran").Where("waktu_transaksi >= ? AND waktu_transaksi < ? AND user_id = ? AND deleted_at is NULL", roundedBawah, roundedAtas, user).Scan(&objs).Error; err != nil {
		if errors.Is(gorm.ErrRecordNotFound, err) {
			return statsTransaction{}, nil
		}
		return statsTransaction{}, err
	}
	return objs, nil
}

func StatsTransactionPaginationbyUserID(userID uuid.UUID) (statsTransaction, error) {
	var (
		objs statsTransaction
	)

	db := db.GetDB()

	if tx := db.Table("transaksi_batch").Select("sum(penjualan) as penjualan, sum(pengeluaran) as pengeluaran").Where("user_id = ? AND deleted_at is NULL", userID).Scan(&objs); tx.Error != nil {
		if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
			return statsTransaction{}, nil
		}
		return objs, errors.New("transaction fail")
	}
	return objs, nil
}
