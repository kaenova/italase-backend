package models

import (
	"errors"
	"fmt"
	"io"
	"mime/multipart"
	"os"
	"path"
	"src/db"
	"src/entity"
	"strings"

	"github.com/google/uuid"
	"gorm.io/gorm/clause"
)

func UserSearchID(id string) (entity.User, error) {
	var (
		obj entity.User
	)
	db := db.GetDB()
	if res := db.Where("id = ?", id).First(&obj); res.Error != nil || res.RowsAffected == 0 {
		if res.Error != nil {
			return entity.User{}, res.Error
		}
		return entity.User{}, errors.New("tidak ditemukan user yang diminta")
	}
	return obj, nil
}

func UserSearchName(name string) (interface{}, error) {
	var (
		mulObj []entity.User
	)
	db := db.GetDB()
	if res := db.Select("nama", "email").Where("nama LIKE ?", fmt.Sprintf("%%%s%%", name)).Find(&mulObj); res.Error != nil || res.RowsAffected == 0 {
		if res.Error != nil {
			return nil, res.Error
		}
		return "tidak ditemukan user yang diminta", nil
	}
	return mulObj, nil
}

func UserSearchEmailAuth(email string) (entity.User, error) {
	var (
		obj entity.User
	)
	db := db.GetDB()
	if res := db.Where("email = ?", email).First(&obj); res.Error != nil || res.RowsAffected == 0 {
		if res.Error != nil {
			return entity.User{}, res.Error
		}
		return entity.User{}, errors.New("email tidak ditemukan")
	}
	return obj, nil
}

func UserSearchEmail(email string) (entity.User, error) {
	var (
		obj entity.User
	)
	db := db.GetDB()
	if res := db.Select("user_id", "nama", "email").Where("email = ?", email).First(&obj); res.Error != nil || res.RowsAffected == 0 {
		if res.Error != nil {
			return entity.User{}, res.Error
		}
		return entity.User{}, errors.New("email tidak ditemukan")
	}
	return obj, nil
}

func UserCreateUser(name, email, password string) (interface{}, error) {
	var obj entity.User

	db := db.GetDB()

	result := db.Where("email = ?", email).First(&obj)
	if result.Error == nil || result.RowsAffected != 0 || obj.Email == email {
		return nil, errors.New("email sudah digunakan")
	}

	user := entity.User{Nama: name, Email: email, Password: password}

	result = db.Create(&user)
	if result.Error != nil || result.RowsAffected == 0 {
		return nil, result.Error
	}

	user.Password = ""

	return user, nil
}

func UserUpdateUser(data *entity.User, files []*multipart.FileHeader) error {
	var imageUpdaate bool = false
	var imagePath string = ""
	var user entity.User
	db := db.GetDB()

	// Save photo if any
	if len(files) != 0 {
		// Check photo saved if any
		err := db.Where("id = ?", data.ID).First(&user).Error
		if err != nil {
			return errors.New("transaction error 18")
		}

		// Deleting old photo if any
		if user.ImageUrl != "" {
			fileName := strings.TrimPrefix(user.ImageUrl, "/static/image/")
			e := os.Remove("./static/image/" + fileName)
			if e != nil {
				return errors.New("transaction error 18")
			}
		}

		// Saving new photo
		for i := 0; i < 1; i++ {
			file := files[0]
			// Source
			src, err := file.Open()
			if err != nil {
				return errors.New("transaction error 21")
			}
			defer src.Close()

			// Destination
			fileExtension := path.Ext(file.Filename)
			if fileExtension != ".jpg" && fileExtension != ".png" && fileExtension != ".jpeg" {
				return errors.New("transaction error 20")
			}
			file.Filename = uuid.NewString() + fileExtension
			imagePath = "./static/image/" + file.Filename
			dst, err := os.Create(imagePath)
			if err != nil {
				return err
			}
			defer dst.Close()

			// Copy
			if _, err = io.Copy(dst, src); err != nil {
				return errors.New("transaction error 21")
			}
			data.ImageUrl = "/static/image/" + file.Filename
			imageUpdaate = true
		}
	}

	// Save to DB
	res := db.Clauses(clause.Returning{}).Updates(&data)
	if res.Error != nil {
		if imageUpdaate {
			e := os.Remove(imagePath)
			if e != nil {
				return errors.New("transaction error 22")
			}
		}
		return res.Error
	}
	return nil
}

func UserDeleteUser(id string) error {
	db := db.GetDB()
	// tx := db.Begin()
	// tx.SavePoint("sp1")

	// if err := db.Where()

	if res := db.Where("id = ?", id).Delete(&entity.User{}); res.Error != nil {
		return res.Error
	}

	// tx.Commit()
	return nil
}
