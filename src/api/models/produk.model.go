package models

import (
	"errors"
	"fmt"
	"src/db"
	"src/entity"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

func TambahProduk(obj entity.Produk) (entity.Produk, error) {
	db := db.GetDB()
	// Begin tx
	tx := db.Begin()
	tx.SavePoint("sp1")
	// insert data
	if err := tx.Create(&obj).Error; err != nil {
		tx.RollbackTo("sp1")
		tx.Commit()
		return entity.Produk{}, errors.New("transaction fail")
	}
	// Setup transaction entity
	trans := entity.Transaction{
		BesarPerubahan: int(obj.JumlahStok) - 0,
		ProdukID:       obj.ID,
		JumlahAkhir:    int(obj.JumlahStok),
		NamaProduk:     obj.Nama,
		HargaJual:      obj.HargaJual,
	}
	// insert transaction
	if err := tx.Select("id", "besar_perubahan", "produk_id", "jumlah_akhir", "nama_produk", "harga_jual").Create(&trans).Error; err != nil {
		tx.RollbackTo("sp1")
		tx.Commit()
		return entity.Produk{}, errors.New("transaction fail")
	}
	tx.Commit()
	return obj, nil
}

func GetProdukID(uuid uuid.UUID) (entity.Produk, error) {
	var obj entity.Produk
	db := db.GetDB()
	tx := db.First(&obj, uuid)
	if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
		return entity.Produk{}, gorm.ErrRecordNotFound
	}

	if tx.Error != nil {
		return entity.Produk{}, errors.New("transaction fail")
	}

	return obj, nil
}

func GetAllProduk(userID uuid.UUID, pageNum int) ([]entity.Produk, error) {
	var obj []entity.Produk
	var numPerPage int = 10 // Hardcoded 10 kembalian per request

	db := db.GetDB()
	offset := (pageNum - 1) * numPerPage
	if err := db.Order("nama asc").Where("user_id = ?", userID).Offset(offset).Limit(numPerPage).Find(&obj).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return []entity.Produk{}, nil
		}
		return []entity.Produk{}, errors.New("transaction fail")
	}
	return obj, nil
}

func DeleteProdukID(obj entity.Produk) (entity.Produk, error) {
	var tempObj entity.Produk = obj
	db := db.GetDB()

	tx := db.Delete(&tempObj)
	if tx.Error != nil || tx.RowsAffected == 0 || errors.Is(tx.Error, gorm.ErrRecordNotFound) {
		return obj, errors.New("transaction fail")
	}

	return obj, nil
}

func UpdateProduk(obj entity.Produk, before entity.Produk) (entity.Produk, error) {
	// Setup transaction entity
	trans := entity.Transaction{
		BesarPerubahan: int(obj.JumlahStok) - int(before.JumlahStok),
		ProdukID:       obj.ID,
		JumlahAkhir:    int(obj.JumlahStok),
		NamaProduk:     obj.Nama,
		HargaJual:      obj.HargaJual,
	}
	db := db.GetDB()
	// Begin tx
	tx := db.Begin()
	tx.SavePoint("sp1")
	// insert transaction
	if err := tx.Select("id", "besar_perubahan", "produk_id", "jumlah_akhir", "nama_produk", "harga_jual").Create(&trans).Error; err != nil {
		tx.RollbackTo("sp1")
		tx.Commit()
		return entity.Produk{}, errors.New("transaction fail")
	}
	// insert updated data
	if err := tx.Save(&obj).Error; err != nil {
		tx.RollbackTo("sp1")
		tx.Commit()
		return entity.Produk{}, errors.New("transaction fail")
	}
	tx.Commit()
	return obj, nil
}

func GetAllMinimumProduk(user uuid.UUID, pageNum int) ([]entity.Produk, error) {
	var (
		objs       []entity.Produk
		numPerPage int = 10
	)

	db := db.GetDB()
	offset := (pageNum - 1) * numPerPage

	if err := db.Order("nama asc").Where("jumlah_stok <= stok_minimum AND user_id = ? AND status_kelola_stok = true", user).Offset(offset).Limit(numPerPage).Find(&objs).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return []entity.Produk{}, nil
		}
		return []entity.Produk{}, err
	}
	return objs, nil
}

func GetAllMinimumProdukNoPage(user uuid.UUID) ([]entity.Produk, error) {
	var (
		objs []entity.Produk
	)

	db := db.GetDB()

	if err := db.Order("nama asc").Where("jumlah_stok <= stok_minimum AND user_id = ? AND status_kelola_stok = true", user).Find(&objs).Error; err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			return []entity.Produk{}, nil
		}
		return []entity.Produk{}, err
	}
	return objs, nil
}

func ProdukSearch(cat string, user uuid.UUID, page int) ([]entity.Produk, error) {
	var (
		objs       []entity.Produk
		numPerPage int = 10 // Hardcoded 10 kembalian per request
	)

	db := db.GetDB()
	offset := (page - 1) * numPerPage

	tx := db.Order("nama asc").Where("LOWER(nama) LIKE LOWER(?) AND user_id = ?", fmt.Sprintf("%%%s%%", cat), user).Offset(offset).Limit(numPerPage).Find(&objs)
	if tx.Error != nil {
		if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
			return []entity.Produk{}, nil
		}
		return []entity.Produk{}, tx.Error
	}
	return objs, nil
}

func ProdukSearchMinimum(cat string, user uuid.UUID, page int) ([]entity.Produk, error) {
	var (
		objs       []entity.Produk
		numPerPage int = 10 // Hardcoded 10 kembalian per request
	)

	db := db.GetDB()
	offset := (page - 1) * numPerPage

	tx := db.Order("nama asc").Where("LOWER(nama) LIKE LOWER(?) AND user_id = ? AND jumlah_stok <= stok_minimum", fmt.Sprintf("%%%s%%", cat), user).Offset(offset).Limit(numPerPage).Find(&objs)
	if tx.Error != nil {
		if errors.Is(tx.Error, gorm.ErrRecordNotFound) {
			return []entity.Produk{}, nil
		}
		return []entity.Produk{}, tx.Error
	}

	return objs, nil
}
