package controllers

import (
	"net/http"
	"src/api/models"
	"src/entity"
	"src/utils"
	"strconv"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

func TransactionBatchPost(c echo.Context) error {
	type transReq struct {
		Catatan        string               `json:"catatan"`
		Penjualan      float32              `json:"penjualan"`
		Pengeluaran    float32              `json:"pengeluaran"`
		NamaPelanggan  string               `json:"nama_pelanggan"`
		WaktuTransaksi time.Time            `json:"waktu_transaksi"`
		Transactions   []entity.Transaction `json:"transaksi"`
	}

	var (
		res         models.Response = models.CreateResponse()
		req         transReq
		transaction entity.TransactionBatch
		hutang      entity.Hutang
	)

	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	if err := c.Bind(&req); err != nil {
		res.Message = "Request tidak sesuai, harap baca dokumentasi API " + err.Error()
		return c.JSON(res.Status, res)
	}

	// Request Check
	if req.Penjualan == 0 && req.Pengeluaran == 0 {
		res.Message = "Setidaknya ada penjualan atau pengeluaran"
		return c.JSON(res.Status, res)
	}

	if len(req.Transactions) < 1 {
		res.Message = "Setidaknya ada 1 produk yang ditransaksikan"
		return c.JSON(res.Status, res)
	}

	if strings.TrimSpace(req.NamaPelanggan) != "" {
		hutang.ID = uuid.New()
		hutang.NamaPelanggan = req.NamaPelanggan
		hutang.Catatan = req.Catatan
		hutang.IsMemberi = true
		hutang.JumlahPeminjaman = req.Penjualan
		hutang.UserID = userID
		hutang.WaktuTransaksi = req.WaktuTransaksi
		transaction.IsLunas = false
	} else {
		transaction.IsLunas = true
	}
	// Binding Data
	transaction.Catatan = req.Catatan
	transaction.Penjualan = req.Penjualan
	transaction.Pengeluaran = req.Pengeluaran
	transaction.WaktuTransaksi = req.WaktuTransaksi
	transaction.UserID = userID
	transaction.ID = uuid.New()
	transaction.Transactions = req.Transactions

	err = models.CreateTransactionBatch(&transaction, &hutang)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	res.Data = transaction
	return c.JSON(res.Status, res)
}

func TransactionBatchGet(c echo.Context) error {
	var (
		res     models.Response = models.CreateResponse()
		err     error
		done    bool = false
		pageNum int  = 1
	)

	// Get JWT
	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	// Get Params
	batchID := c.QueryParam("id")
	timeInput := c.QueryParam("time")
	endInput := c.QueryParam("end")
	searchInput := c.QueryParam("search")
	pageInput := c.QueryParam("page")

	// Setting Up Pagination
	if strings.TrimSpace(pageInput) != "" {
		pageNum, err = strconv.Atoi(pageInput)
		if err != nil {
			res.Message = "Page Number tidak valid"
			return c.JSON(res.Status, res)
		}
	}

	/*
		Priority
		1. ID
		2. Search
		3. Pagination by Time
	*/

	// ID
	if strings.TrimSpace(batchID) != "" && !done {
		var data entity.TransactionBatch
		batchUUID, err := uuid.Parse(batchID)
		if err != nil {
			res.Message = "Id tidak valid"
			return c.JSON(res.Status, res)
		}
		data, err = models.BatchTransactionSearchbyID(userID, batchUUID)
		res.Data = data
		if data.ID == uuid.Nil {
			res.Data = nil
			res.Status = http.StatusUnauthorized
			res.Message = "Not Authorized / Not Found"
			return c.JSON(res.Status, res)
		}
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Search
	if strings.TrimSpace(searchInput) != "" && !done {
		res.Data, err = models.BatchTransactionSearchCatatan(searchInput, userID, pageNum)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Pagination
	// Today
	if strings.TrimSpace(timeInput) == "day" && !done {
		res.Data, err = models.BatchTransactionToday(userID, pageNum)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Week
	if strings.TrimSpace(timeInput) == "week" && !done {
		res.Data, err = models.BatchTransactionWeek(userID, pageNum)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Month
	if strings.TrimSpace(timeInput) == "month" && !done {
		res.Data, err = models.BatchTransactionBulan(userID, pageNum)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Custom
	// Satu Tanggal
	if strings.TrimSpace(endInput) == "" && strings.TrimSpace(timeInput) != "" && !done {
		timeNow, err := time.Parse(time.RFC3339, timeInput)
		if err != nil {
			res.Message = "Time tidak valid, sesuaikan dengan RFC3339"
			return c.JSON(res.Status, res)
		}
		res.Data, err = models.BatchTransactionSatuTanggal(timeNow, userID, pageNum)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Range Tanggal
	if strings.TrimSpace(endInput) != "" && strings.TrimSpace(timeInput) != "" && !done {
		timeNow, err1 := time.Parse(time.RFC3339, timeInput)
		endNow, err2 := time.Parse(time.RFC3339, endInput)
		if err1 != nil || err2 != nil {
			res.Message = "Time tidak valid, sesuaikan dengan RFC3339"
			return c.JSON(res.Status, res)
		}
		res.Data, err = models.BatchTransactionRangeTanggal(timeNow, endNow, userID, pageNum)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Semua Dari Hari ini.
	if !done {
		res.Data, err = models.BatchTransactionPaginationbyUserID(userID, pageNum)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	return c.JSON(res.Status, res)
}

func TransactionBatchPut(c echo.Context) error {
	type transReq struct {
		Catatan        string               `json:"catatan"`
		Penjualan      float32              `json:"penjualan"`
		Pengeluaran    float32              `json:"pengeluaran"`
		NamaPelanggan  string               `json:"nama_pelanggan"`
		WaktuTransaksi time.Time            `json:"waktu_transaksi"`
		IsLunas        bool                 `json:"is_lunas"`
		Transactions   []entity.Transaction `json:"transaksi"`
	}

	var (
		res         models.Response = models.CreateResponse()
		transaction entity.TransactionBatch
		req         transReq
		err         error
	)

	idInput := c.QueryParam("id")
	batchID, err := uuid.Parse(idInput)
	if err != nil {
		res.Message = "ID tidak valid"
		return c.JSON(res.Status, res)
	}

	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	// Binding Batch Transaction Request
	if err := c.Bind(&req); err != nil {
		res.Message = "Request tidak sesuai, harap baca dokumentasi API " + err.Error()
		return c.JSON(res.Status, res)
	}

	if (!req.IsLunas) && (strings.TrimSpace(req.NamaPelanggan) == "") {
		res.Message = "Masukkan nama pelanggan jika ingin membuat hutang"
		return c.JSON(res.Status, res)
	}

	transaction.UserID = userID
	transaction.ID = batchID
	transaction.Catatan = req.Catatan
	transaction.Penjualan = req.Penjualan
	transaction.Pengeluaran = req.Pengeluaran
	transaction.WaktuTransaksi = req.WaktuTransaksi
	transaction.Transactions = req.Transactions
	transaction.IsLunas = req.IsLunas

	if len(req.Transactions) <= 0 {
		res.Message = "Request tidak sesuai, harap baca dokumentasi API " + err.Error()
		return c.JSON(res.Status, res)
	}

	// Updating to models
	transaction, err = models.UpdateBatchTransaction(transaction, req.NamaPelanggan)
	if err != nil {
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Data = transaction
	res.Status = http.StatusOK
	res.Message = "Success"
	return c.JSON(res.Status, res)
}

func TransactionBatchDelete(c echo.Context) error {
	var (
		res models.Response = models.CreateResponse()
	)
	// Get User ID
	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	idTransactionQuery := c.QueryParam("id")
	idTransaction, err := uuid.Parse(idTransactionQuery)
	// KETIKA DATA TIDAK VALID
	if err != nil {
		res.Message = "ID tidak valid"
		return c.JSON(res.Status, res)
	}

	if err != nil {
		res.Status = http.StatusBadRequest
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	// Delete
	data, err := models.DeleteTransactionID(idTransaction, userID)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Data = data
	res.Status = http.StatusOK
	res.Message = "Sukses"
	return c.JSON(res.Status, res)
}
