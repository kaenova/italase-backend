package controllers

import (
	"net/http"
	"src/api/models"
	"src/entity"
	"src/utils"
	"strconv"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

// Create Hutang is not used in this version
// func HutangPost(c echo.Context) error {
// 	var (
// 		res models.Response = models.CreateResponse()
// 		// req  entity.Hutang
// 		err  error
// 		done bool = false
// 	)

// 	// Get JWT Data
// 	userData := c.Get("user").(*jwt.Token)
// 	claims := userData.Claims.(*utils.JWTCustomClaims)
// 	userID, err := uuid.Parse(claims.Id)
// 	if err != nil {
// 		res.Status = http.StatusInternalServerError
// 		res.Message = "Gagal parsing JWT"
// 		return c.JSON(res.Status, res)
// 	}

// 	// Get Query Data
// 	idInput := c.QueryParam("id")

// 	/*
// 		Priority:
// 			1. Lunas hutang by ID
// 			2. Creaet Hutang
// 	*/

// 	if strings.TrimSpace(idInput) != "" && !done {
// 		idLunas, err := uuid.Parse(idInput)
// 		if err != nil {
// 			res.Message = "ID is not valid"
// 			return c.JSON(res.Status, res)
// 		}
// 		res.Data, err = models.HutangLunas(idLunas, userID)
// 		if err != nil {
// 			res.Message = err.Error()
// 			res.Status = http.StatusInternalServerError
// 			return c.JSON(res.Status, res)
// 		}
// 		done = true
// 	}

// 	// // Binding Batch Hutang Request
// 	// if err := c.Bind(&req); err != nil {
// 	// 	res.Message = "Request tidak sesuai, harap baca dokumentasi API " + err.Error()
// 	// 	return c.JSON(res.Status, res)
// 	// }
// 	// req.UserID = userID

// 	// // Checking
// 	// if strings.TrimSpace(req.NamaPelanggan) == "" {
// 	// 	res.Message = "Nama Pelanggan tidak boleh kosong"
// 	// 	return c.JSON(res.Status, res)
// 	// }
// 	// if req.JumlahPeminjaman <= 0 {
// 	// 	res.Message = "Jumlah Peminjaman tidak valid"
// 	// 	return c.JSON(res.Status, res)
// 	// }

// 	// // Masukkan ke database
// 	// err = models.AddHutang(&req)
// 	// if err != nil {
// 	// 	res.Status = http.StatusInternalServerError
// 	// 	res.Message = err.Error()
// 	// 	return c.JSON(res.Status, res)
// 	// }

// 	res.Message = "Success"
// 	res.Status = http.StatusOK
// 	return c.JSON(res.Status, res)
// }

func HutangGet(c echo.Context) error {
	var (
		res     models.Response = models.CreateResponse()
		done    bool
		pageNum int = 1
	)

	// Get JWT Data
	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	// Get Params
	hutangID := c.QueryParam("id")
	pageInput := c.QueryParam("page")
	searchInput := c.QueryParam("search")

	/*
		Priority
		1. ID
		2. Search
		3. Pagination
	*/

	// ID
	if strings.TrimSpace(hutangID) != "" && !done {
		var hutang entity.Hutang
		hutangUUID, err := uuid.Parse(hutangID)
		if err != nil {
			res.Message = "Id tidak valid"
			return c.JSON(res.Status, res)
		}
		hutang, err = models.HutangSearchbyID(userID, hutangUUID)
		res.Data = hutang
		if hutang.ID == uuid.Nil {
			res.Data = nil
			res.Status = http.StatusUnauthorized
			res.Message = "Not Authorized / Not Found"
			return c.JSON(res.Status, res)
		}
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	if !done {
		pageNum, err = strconv.Atoi(pageInput)
		if err != nil {
			res.Message = "Page tidak valid"
			return c.JSON(res.Status, res)
		}
	}

	// Search by Nama Pelanggan
	if strings.TrimSpace(searchInput) != "" && !done {
		res.Data, err = models.HutangSearchbyNama(searchInput, userID, pageNum)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Pagination
	if !done {
		res.Data, err = models.HutangPagination(userID, pageNum)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	res.Message = "Success"
	res.Status = http.StatusOK
	return c.JSON(res.Status, res)
}

func HutangDelete(c echo.Context) error {
	var (
		res models.Response = models.CreateResponse()
	)

	// Get Params
	hutangInput := c.QueryParam("id")

	// Get JWT Data
	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	// Checking
	hutangID, err := uuid.Parse(hutangInput)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal Parsing Query ID"
		return c.JSON(res.Status, res)
	}

	// Deleting
	res.Data, err = models.HutangDelete(userID, hutangID)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Message = "Success"
	res.Status = http.StatusOK
	return c.JSON(res.Status, res)
}

func HutangPut(c echo.Context) error {
	var (
		res models.Response = models.CreateResponse()
		req entity.Hutang
	)

	// Get Params
	hutangInput := c.QueryParam("id")

	// Get JWT Data
	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	// Binding Batch Hutang Request
	if err := c.Bind(&req); err != nil {
		res.Message = "Request tidak sesuai, harap baca dokumentasi API " + err.Error()
		return c.JSON(res.Status, res)
	}
	//// Checking before binding
	hutangID, err := uuid.Parse(hutangInput)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal Parsing Query ID"
		return c.JSON(res.Status, res)
	}
	req.UserID = userID
	req.ID = hutangID

	// Checking
	if strings.TrimSpace(req.NamaPelanggan) == "" {
		res.Message = "Nama Pelanggan tidak boleh kosong"
		return c.JSON(res.Status, res)
	}
	if req.JumlahPeminjaman <= 0 {
		res.Message = "Jumlah Peminjaman tidak valid"
		return c.JSON(res.Status, res)
	}

	res.Data, err = models.HutangUbah(req)
	if err != nil {
		res.Message = err.Error()
		res.Status = http.StatusInternalServerError
		return c.JSON(res.Status, res)
	}

	res.Message = "Success"
	res.Status = http.StatusOK
	return c.JSON(res.Status, res)
}

func HutangStatsGet(c echo.Context) error {
	var (
		res models.Response = models.CreateResponse()
	)

	// Get JWT Data
	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	res.Data, err = models.HutangStats(userID)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Message = "Success"
	res.Status = http.StatusOK
	return c.JSON(res.Status, res)
}
