package controllers

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"src/api/models"
	"src/entity"
	"src/utils"
	"strconv"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

func ProdukGet(c echo.Context) error {
	var (
		res     models.Response = models.CreateResponse()
		err     error
		minBool bool   = false
		done           = false
		pageNum int    = 1
		search  string = ""
	)

	// ambil id user untuk otorisasi data produk usernya
	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	// ambil semua query
	id := c.QueryParam("id")
	minimum := c.QueryParam("minimum")
	pageQuery := c.QueryParam("page")
	searchInput := c.QueryParam("search")

	/*
		Priority
		1. ID (no page)
		2. Menipis (page minimum = true) bisa menggunakan (search)
		3. ALl (page) bisa menggunakan (search)

		2 dan 3 wajib menggunakan page, lakukan check page dulu
	*/

	// ID
	if strings.TrimSpace(id) != "" && !done {
		uuidProduk, err := uuid.Parse(id)
		if err != nil {
			res.Status = http.StatusBadRequest
			res.Message = "ID tidak valid"
			return c.JSON(res.Status, res)
		}
		data, err := models.GetProdukID(uuidProduk)
		// Ketika ada masalah dalam transaksi db
		if err != nil && !errors.Is(gorm.ErrRecordNotFound, err) {
			res.Status = http.StatusBadRequest
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		// Ketika bukan abrang kepemilikannya atau tidak ditemukan
		if data.UserID != userID || (data == entity.Produk{}) {
			res.Status = http.StatusUnauthorized
			res.Message = "Not Authorized / Not Found"
			return c.JSON(res.Status, res)
		}
		if data.ID == uuid.Nil {
			res.Data = nil
		}
		res.Data = data
		done = true
	}

	// Check Page untuk Menipis dan All
	if strings.TrimSpace(pageQuery) != "" && !done {
		if pageQuery != "all" {
			pageNum, err = strconv.Atoi(pageQuery)
			if err != nil || pageNum < 1 {
				res.Message = "Failed to parse page"
				return c.JSON(res.Status, res)
			}
		}
	}

	// Chcek Apakah dia ingin minimum?
	if strings.TrimSpace(minimum) != "" && !done {
		minBool, err = strconv.ParseBool(minimum)
		if err != nil {
			res.Message = "Failed to parse minimum"
			return c.JSON(res.Status, res)
		}
	}

	// Ceheck apakah ingin search Produk
	if strings.TrimSpace(searchInput) != "" && !done {
		search = searchInput
	}

	// Minimum
	if minBool && !done {
		if search != "" {
			res.Data, err = models.ProdukSearchMinimum(search, userID, pageNum)
		} else if pageQuery == "all" {
			res.Data, err = models.GetAllMinimumProdukNoPage(userID)
		} else {
			res.Data, err = models.GetAllMinimumProduk(userID, pageNum)
		}
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Get All
	if !done {
		if search != "" {
			res.Data, err = models.ProdukSearch(search, userID, pageNum)
		} else {
			res.Data, err = models.GetAllProduk(userID, pageNum)
		}
		if err != nil {
			res.Status = http.StatusBadRequest
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	res.Status = http.StatusOK
	res.Message = "Sukses"
	return c.JSON(res.Status, res)
}

func ProdukPost(c echo.Context) error {
	var (
		res models.Response = models.CreateResponse()
		obj entity.Produk
	)

	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}
	obj.UserID = userID

	// Membaca JSON dan memasukkan ke tipe bentukan
	payload, _ := ioutil.ReadAll(c.Request().Body)
	err = json.Unmarshal(payload, &obj)
	if err != nil {
		res.Message = "Request tidak sesuai, harap baca dokumentasi API"
		return c.JSON(res.Status, res)
	}

	// Error check on string
	if strings.TrimSpace(obj.Nama) == "" {
		res.Status = http.StatusBadRequest
		res.Message = "Nama tidak boleh kosong"
		return c.JSON(res.Status, res)
	}

	// Inputting to DB
	res.Data, err = models.TambahProduk(obj)
	if err != nil {
		res.Status = http.StatusBadRequest
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Status = http.StatusOK
	res.Message = "Sukses"
	return c.JSON(res.Status, res)
}

func ProdukDelete(c echo.Context) error {
	var (
		res models.Response = models.CreateResponse()
	)
	// Get User ID
	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	idProdukQuery := c.QueryParam("id")
	idProduk, err := uuid.Parse(idProdukQuery)
	// Gagal parsing idProduk
	if err != nil {
		res.Message = "ID tidak valid"
		return c.JSON(res.Status, res)
	}

	// Ambil data Produk
	data, err := models.GetProdukID(idProduk)
	// Ketika ada masalah dalam transaksi db
	if err != nil {
		res.Status = http.StatusBadRequest
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	// Ketika bukan abrang kepemilikannya atau tidak ditemukan
	if data.UserID != userID || (data == entity.Produk{}) {
		res.Status = http.StatusUnauthorized
		res.Message = "Not Authorized / Not Found"
		return c.JSON(res.Status, res)
	}

	// Delete Produk
	data, err = models.DeleteProdukID(data)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Data = data
	res.Status = http.StatusOK
	res.Message = "Sukses"
	return c.JSON(res.Status, res)
}

func ProdukUpdate(c echo.Context) error {
	var (
		res models.Response = models.CreateResponse()
		obj entity.Produk
	)

	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	idProdukQuery := c.QueryParam("id")
	idProduk, err := uuid.Parse(idProdukQuery)
	// Gagal parsing idProduk
	if err != nil {
		res.Message = "ID Produk tidak sesuai"
		return c.JSON(res.Status, res)
	}

	// Masukkan data
	payload, _ := ioutil.ReadAll(c.Request().Body)
	err = json.Unmarshal(payload, &obj)
	if err != nil {
		res.Message = "Request tidak sesuai, harap baca dokumentasi API"
		return c.JSON(res.Status, res)
	}
	obj.UserID = userID
	obj.ID = idProduk

	// Error check on string
	if strings.TrimSpace(obj.Nama) == "" {
		res.Status = http.StatusBadRequest
		res.Message = "Nama tidak boleh kosong"
		return c.JSON(res.Status, res)
	}

	// Ambil data Produk
	data, err := models.GetProdukID(idProduk)
	// Ketika ada masalah dalam transaksi db
	if err != nil {
		res.Status = http.StatusBadRequest
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	// Ketika bukan abrang kepemilikannya atau tidak ditemukan
	if data.UserID != userID || data.ID != obj.ID || (data == entity.Produk{}) {
		res.Status = http.StatusUnauthorized
		res.Message = "Not Authorized / Not Found"
		return c.JSON(res.Status, res)
	}

	obj.CreatedAt = data.CreatedAt

	// Changing onto DB
	res.Data, err = models.UpdateProduk(obj, data)
	if err != nil {
		res.Status = http.StatusBadRequest
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Status = http.StatusOK
	res.Message = "Sukses"
	return c.JSON(res.Status, res)
}
