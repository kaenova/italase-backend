package controllers

import (
	"net/http"
	"src/api/models"
	"src/utils"
	"strings"
	"time"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
)

func TransactionStatsGet(c echo.Context) error {
	var (
		res  models.Response = models.CreateResponse()
		err  error
		done bool = false
	)

	// Get JWT
	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID, err := uuid.Parse(claims.Id)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Gagal parsing JWT"
		return c.JSON(res.Status, res)
	}

	// Get Params
	timeInput := c.QueryParam("time")
	endInput := c.QueryParam("end")
	searchInput := c.QueryParam("search")

	// Search
	if strings.TrimSpace(searchInput) != "" && !done {
		res.Data, err = models.StatsTransactionSearchCatatan(searchInput, userID)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Pagination
	// Today
	if strings.TrimSpace(timeInput) == "day" && !done {
		res.Data, err = models.StatsTransactionToday(userID)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Week
	if strings.TrimSpace(timeInput) == "week" && !done {
		res.Data, err = models.StatsTransactionWeek(userID)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Month
	if strings.TrimSpace(timeInput) == "month" && !done {
		res.Data, err = models.StatsTransactionBulan(userID)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Custom
	// Satu Tanggal
	if strings.TrimSpace(endInput) == "" && strings.TrimSpace(timeInput) != "" && !done {
		timeNow, err := time.Parse(time.RFC3339, timeInput)
		if err != nil {
			res.Message = "Time tidak valid, sesuaikan dengan RFC3339"
			return c.JSON(res.Status, res)
		}
		res.Data, err = models.StatsTransactionSatuTanggal(timeNow, userID)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Range Tanggal
	if strings.TrimSpace(endInput) != "" && strings.TrimSpace(timeInput) != "" && !done {
		timeNow, err1 := time.Parse(time.RFC3339, timeInput)
		endNow, err2 := time.Parse(time.RFC3339, endInput)
		if err1 != nil || err2 != nil {
			res.Message = "Time tidak valid, sesuaikan dengan RFC3339"
			return c.JSON(res.Status, res)
		}
		res.Data, err = models.StatsTransactionRangeTanggal(timeNow, endNow, userID)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	// Semua Dari Hari ini.
	if !done {
		res.Data, err = models.StatsTransactionPaginationbyUserID(userID)
		if err != nil {
			res.Status = http.StatusInternalServerError
			res.Message = err.Error()
			return c.JSON(res.Status, res)
		}
		done = true
	}

	res.Status = http.StatusOK
	res.Message = "Success"
	return c.JSON(res.Status, res)
}
