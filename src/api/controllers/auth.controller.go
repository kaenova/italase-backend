package controllers

import (
	"errors"
	"net/http"
	"net/mail"
	"path"
	"src/api/models"
	"src/entity"
	"src/utils"
	"strings"

	"github.com/golang-jwt/jwt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gorm.io/gorm"
)

type userRequest struct {
	Nama     string `json:"nama"`
	Email    string `json:"email"`
	Password string `json:"password"`
}

type userRequestLogin struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}

func GetUser(c echo.Context) error {
	var (
		result models.Response = models.CreateResponse()
		err    error           = nil
	)

	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID := claims.Id

	result.Data, err = models.UserSearchID(userID)
	if err != nil {
		result.Message = err.Error()
		result.Status = http.StatusInternalServerError
		return c.JSON(result.Status, result)
	}

	result.Status = http.StatusOK
	result.Message = "Success"
	return c.JSON(result.Status, result)
}

func PostUser(c echo.Context) error {
	var err error

	input := new(userRequest)
	if err := c.Bind(input); err != nil {
		return echo.NewHTTPError(http.StatusBadRequest, err.Error())
	}
	var (
		res models.Response = models.CreateResponse()
	)

	if strings.TrimSpace(input.Nama) == "" || strings.TrimSpace(input.Email) == "" || strings.TrimSpace(input.Password) == "" {
		res.Status = http.StatusBadRequest
		res.Message = "Request tidak lengkap."
		return c.JSON(res.Status, res)
	}

	if _, err := mail.ParseAddress(input.Email); err != nil {
		res.Status = http.StatusBadRequest
		res.Message = "Email tidak valid"
		return c.JSON(res.Status, res)
	}

	input.Password, err = utils.HashPassword(input.Password)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = "Failed on processing password"
		return c.JSON(res.Status, res)
	}

	res.Data, err = models.UserCreateUser(input.Nama, input.Email, input.Password)
	if err != nil {
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Status = http.StatusOK
	res.Message = "success"
	return c.JSON(res.Status, res)

}

func LoginUser(c echo.Context) error {
	var (
		user  entity.User
		token string
		req   userRequestLogin

		res models.Response = models.CreateResponse()
		err error           = nil
	)

	if err := c.Bind(&req); err != nil {
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	user, err = models.UserSearchEmailAuth(req.Email)
	if err != nil {
		if errors.Is(err, gorm.ErrRecordNotFound) {
			res.Message = "Tidak Terdaftar"
			return c.JSON(res.Status, res)
		}
		res.Status = http.StatusInternalServerError
		res.Message = "Internal Error"
		return c.JSON(res.Status, res)
	}
	if !utils.CompareHashPassword(req.Password, user.Password) {
		res.Message = "Password Salah"
		return c.JSON(res.Status, res)
	}

	id := user.ID.String()

	token, err = utils.GenerateToken(user.Nama, user.Email, id)
	if err != nil {
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	user.Password = ""
	res.Data = echo.Map{
		"token": token,
		"user":  user,
	}
	res.Status = http.StatusOK
	res.Message = "Success"
	return c.JSON(res.Status, res)
}

func UpdateUser(c echo.Context) error {
	var (
		req entity.User
		res models.Response = models.CreateResponse()
		err error
	)

	// Get JWT Data
	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)

	// Bind Request
	if err := c.Bind(&req); err != nil {
		res.Message = "Request tidak sesuai, periksa dokumentasi API"
		return c.JSON(res.Status, res)
	}
	form, err := c.MultipartForm()
	if err != nil {
		return c.JSON(res.Status, res)
	}
	files := form.File["image"]

	req.ID, err = uuid.Parse(claims.Id)
	if err != nil {
		res.Message = "Invalid ID"
		return c.JSON(res.Status, res)
	}

	// Check file yang di upload
	if len(files) != 0 {
		fileExtension := path.Ext(files[0].Filename)
		if fileExtension != ".jpg" && fileExtension != ".png" && fileExtension != ".jpeg" {
			res.Message = "File yang diterima hanya boleh bertipe file (.jpg .png .jpeg)"
			return c.JSON(res.Status, res)
		}
		if files[0].Size > 1024*1024 {
			res.Message = "Ukuran file terlalu besar"
			return c.JSON(res.Status, res)
		}
	}

	err = models.UserUpdateUser(&req, files)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	req.Password = ""

	res.Data = req
	res.Status = http.StatusOK
	res.Message = "success"
	return c.JSON(res.Status, res)
}

func DeleteUser(c echo.Context) error {
	var (
		res models.Response = models.CreateResponse()
	)

	userData := c.Get("user").(*jwt.Token)
	claims := userData.Claims.(*utils.JWTCustomClaims)
	userID := claims.Id

	err := models.UserDeleteUser(userID)
	if err != nil {
		res.Status = http.StatusInternalServerError
		res.Message = err.Error()
		return c.JSON(res.Status, res)
	}

	res.Message = "Success"
	res.Status = http.StatusOK
	return c.JSON(res.Status, res)
}
