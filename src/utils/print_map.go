package utils

import (
	"encoding/json"
	"fmt"
	"src/utils/errlogger"
)

func PrintMaptoJSON(data interface{}) {
	jsonData, err := json.Marshal(data)
	errlogger.ErrFatalPanic(err)
	fmt.Println(string(jsonData))
}
