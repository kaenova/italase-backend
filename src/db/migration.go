package db

import (
	"src/entity"
	"src/utils/errlogger"

	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

func Migration(db *gorm.DB) {
	/*
		Please fill the params in AutoMigrate with your entity
		so you will see
		db.AutoMigrate(&Entity1{}, &Entity2{}, &Entity3{}, ...)
	*/

	log.Info().Msg("memulai dengan automigrate")

	err := db.AutoMigrate(&entity.Produk{}, &entity.User{}, &entity.Hutang{}, &entity.TransactionBatch{}, &entity.Transaction{})

	// err := db.AutoMigrate(&entity.Produk{}, &entity.User{})
	// err := db.AutoMigrate(&User{}, &CreditCard{})

	errlogger.ErrFatalPanic(err)
}

type User struct {
	gorm.Model
	CreditCards []CreditCard
}

type CreditCard struct {
	gorm.Model
	Number string
	UserID uint
}
