package db

import (
	"src/entity"
	"src/utils/errlogger"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"gorm.io/gorm"
)

func initData(db *gorm.DB) {
	/*
		Use this function to make a initial data.
		You need to initialize your data first and the loop through the data.
		To Create Record please refer reading this https://gorm.io/docs/create.html
	*/

	// Init data user
	uuidDummy, err := uuid.Parse("c9a62f21-36e4-4846-af4a-8c3390a03b3b")
	errlogger.ErrFatalPanic(err)
	obj := entity.User{
		ID:        uuidDummy,
		Nama:      "Test User",
		Email:     "test@gits.com",
		NamaUsaha: "bruh",
		Password:  "kaenova",
		TipeUsaha: "gatau",
	}
	db.Session(&gorm.Session{SkipHooks: true}).Create(&obj)

	log.Info().Msg("dummy data terinisialisasi")
}
