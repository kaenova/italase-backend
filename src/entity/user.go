package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type User struct {
	ID        uuid.UUID `gorm:"type:uuid;primary_key;auto_increment" json:"id_user"`
	Nama      string    `gorm:"not null" json:"nama" form:"nama"`
	Email     string    `gorm:"index;not null" json:"email" form:"email"`
	Password  string    `gorm:"not null" json:"password,omitempty" `
	NamaUsaha string    `gorm:"not null" json:"nama_usaha" form:"nama_usaha"`
	TipeUsaha string    `gorm:"not null" json:"tipe_usaha" form:"tipe_usaha"`
	ImageUrl  string    `gorm:"" json:"image" form:"image"`

	CreatedAt time.Time      `gorm:"not null" json:"created_at"`
	UpdatedAt time.Time      `gorm:"" json:"updated_at"`
	DeletedAt gorm.DeletedAt `json:"deleted_at,omitempty"`
}

func (User) TableName() string {
	return "user"
}

func (u *User) BeforeCreate(tx *gorm.DB) (err error) {
	u.ID = uuid.New()
	return
}
