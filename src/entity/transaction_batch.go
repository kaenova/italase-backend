package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type TransactionBatch struct {
	ID             uuid.UUID      `gorm:"type:uuid;primary_key" json:"id_batch"`
	Catatan        string         `json:"catatan"`
	Penjualan      float32        `gorm:"not null" json:"penjualan"`
	Pengeluaran    float32        `gorm:"not null" json:"pengeluaran"`
	IsLunas        bool           `gorm:"not null" json:"is_lunas"`
	WaktuTransaksi time.Time      `gorm:"index:idx_user_time,priority:2;not null" json:"waktu_transaksi"`
	CreatedAt      time.Time      `gorm:"not null" json:"created_at"`
	UpdatedAt      time.Time      `gorm:"" json:"updated_at"`
	DeletedAt      gorm.DeletedAt `json:"deleted_at,omitempty"`

	UserID       uuid.UUID     `gorm:"index:idx_user_time,priority:1" json:"id_user"`
	User         *User         `json:"user,omitempty"`
	Transactions []Transaction `gorm:"ForeignKey:BatchID" json:"transaksi"`
	HutangID     *uuid.UUID    `json:"id_hutang"`
	Hutang       *Hutang       `json:"hutang"`
}

func (TransactionBatch) TableName() string {
	return "transaksi_batch"
}

func (b *TransactionBatch) BeforeCreate(tx *gorm.DB) (err error) {
	b.CreatedAt = time.Now()
	return
}

func (b *TransactionBatch) BeforeUpdate(tx *gorm.DB) (err error) {
	b.UpdatedAt = time.Now()
	return
}
