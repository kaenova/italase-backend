package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Hutang struct {
	ID               uuid.UUID      `gorm:"type:uuid;primary_key" json:"id_hutang"`
	NamaPelanggan    string         `gorm:"not null" json:"nama_pelanggan"`
	Catatan          string         `json:"catatan"`
	JumlahPeminjaman float32        `gorm:"not null" json:"jumlah_peminjaman"`
	IsMemberi        bool           `gorm:"index;not null" json:"is_memberi"`
	WaktuTransaksi   time.Time      `gorm:"index:idx_hutang_user_time,priority:2;not null" json:"waktu_transaksi"`
	CreatedAt        time.Time      `gorm:"type:timestamptz;not null" json:"created_at"`
	UpdatedAt        time.Time      `gorm:"type:timestamptz;" json:"updated_at"`
	DeletedAt        gorm.DeletedAt `json:"deleted_at,omitempty"`

	UserID uuid.UUID `gorm:"index:idx_hutang_user_time,priority:1" json:"id_user"`
	User   *User     `json:"user,omitempty"`
}

func (Hutang) TableName() string {
	return "hutang"
}

func (b *Hutang) BeforeCreate(tx *gorm.DB) (err error) {
	b.ID = uuid.New()
	b.CreatedAt = time.Now()
	return
}

func (b *Hutang) BeforeUpdate(tx *gorm.DB) (err error) {
	b.UpdatedAt = time.Now()
	return
}
