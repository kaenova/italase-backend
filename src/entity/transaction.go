package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Transaction struct {
	ID             uuid.UUID      `gorm:"type:uuid;primary_key" json:"id_transaction"`
	BesarPerubahan int            `gorm:"not null" json:"besar_perubahan"`
	JumlahAkhir    int            `gorm:"not null" json:"jumlah_akhir"`
	NamaProduk     string         `gorm:"not null" json:"nama_produk"`
	HargaJual      float32        `gorm:"not null" json:"harga_jual"`
	CreatedAt      time.Time      `gorm:"type:timestamptz;not null" json:"created_at"`
	DeletedAt      gorm.DeletedAt `json:"deleted_at,omitempty"`

	ProdukID uuid.UUID `gorm:"index;not_null" json:"id_produk"`
	Produk   *Produk   `json:"produk,omitempty"`
	BatchID  uuid.UUID `gorm:"index" json:"id_batch"`
}

func (Transaction) TableName() string {
	return "transaksi"
}

func (b *Transaction) BeforeCreate(tx *gorm.DB) (err error) {
	b.ID = uuid.New()
	b.CreatedAt = time.Now()
	return
}
