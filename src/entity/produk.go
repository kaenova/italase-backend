package entity

import (
	"time"

	"github.com/google/uuid"
	"gorm.io/gorm"
)

type Produk struct {
	ID               uuid.UUID      `gorm:"type:uuid;primary_key" json:"id_produk"`
	Nama             string         `gorm:"not null" json:"nama_produk"`
	HargaJual        float32        `gorm:"not null" json:"harga_jual"`
	HargaModal       float32        `gorm:"not null" json:"harga_modal"`
	JumlahStok       uint           `gorm:"not null" json:"jumlah_stok"`
	StokMinimum      uint           `gorm:"not null" json:"stok_minimum"`
	Satuan           string         `gorm:"not null; default: 'barang'" json:"satuan"`
	StatusKelolaStok bool           `gorm:"not null; default:false" json:"status_kelola"`
	CreatedAt        time.Time      `gorm:"type:timestamptz;not null" json:"created_at"`
	UpdatedAt        time.Time      `gorm:"type:timestamptz;" json:"updated_at"`
	DeletedAt        gorm.DeletedAt `json:"deleted_at,omitempty"`

	UserID uuid.UUID `json:"id_user"`
	User   *User     `json:"user,omitempty"`
}

func (Produk) TableName() string {
	return "produk"
}

func (b *Produk) BeforeCreate(tx *gorm.DB) (err error) {
	b.ID = uuid.New()
	b.CreatedAt = time.Now()
	return
}

func (b *Produk) BeforeUpdate(tx *gorm.DB) (err error) {
	b.UpdatedAt = time.Now()
	return
}
